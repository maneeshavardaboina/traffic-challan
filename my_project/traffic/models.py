from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

class Vehicle(models.Model):
    Vehicle_Number = models.CharField(max_length=25)
    Model_Name = models.CharField(max_length=256)
    Vehicle_Type = models.CharField(max_length=256)
    Owner_Name = models.ForeignKey(User, on_delete=models.CASCADE)
    
    def __str__(self) -> str:
        return self.Vehicle_Number
    
class Rules(models.Model):
    Rule_id = models.IntegerField(default=0)
    Rule_Name = models.CharField(max_length=256)
    Fine_Amount = models.IntegerField(default=0)
    
    def __str__(self):
        return self.Rule_Name
     
class Issue_Challan(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    Date = models.DateTimeField('date Issued', auto_now=True)
    Place = models.CharField(max_length=256)
    Issued_by = models.ForeignKey(User, on_delete=models.CASCADE)
    Fine_Amount =models.IntegerField()
    Rule_Name = models.ForeignKey(Rules, on_delete=models.CASCADE)
    upload_image = models.ImageField(null=True,blank=True,upload_to='images/')
    
    
    
class PolicePersonal(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    Phone_no = models.IntegerField(default=0)
    Emp_Id = models.IntegerField(default=0)
    Designation = models.CharField(max_length=256)
    
    

class Owner(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    Phone_no = models.IntegerField(default=0)
    Licence = models.CharField(max_length=256)




    

    


    
