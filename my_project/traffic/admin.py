from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register([Issue_Challan, Vehicle, Owner, PolicePersonal, Rules])
