from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('view',views.challan_list,name='challan'),
    path('issue',views.issue_challan,name='issue_challan'),
    path('login', views.login_, name='login'),
    path('logout', views.logout_, name='logout'),
    path('challan_form', views.challan_form, name='challan-form'),
    path('echallan',views.e_challan,name='e-challan'),
    path('viewing',views.view_challan,name='viewing'),
    path('e_contact',views.e_contact,name='e_contact'),
    path('vehicledetails',views.vehicle_details, name='vehicle_details'),
    path('challanhistory',views.challanhistory,name='challan-history'),
    path('registered',views.registeredvehicles,name='registered-vehicles'),
    path('paychallan',views.pay_challan,name='pay-challan')
    
]+ static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)