from django.shortcuts import render,get_object_or_404,redirect
from .models import Issue_Challan,Vehicle,Owner, PolicePersonal, Rules
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, permission_required
from .forms import ChallanForm
from django.http import HttpResponseRedirect,HttpResponse
@login_required(login_url = 'login')    

def issue_challan(request):
    return render(request,'issuechallanpage.html')


def challan_list(request):
    return render(request, 'challan.html')


def login_(request):
    if request.method == 'GET':
        return render(request, 'authenticate.html')
    if request.method == 'POST':
        username_ = request.POST.get('username', '')
        entered_password = request.POST.get('password', '')
        user = authenticate(username=username_, password=entered_password)
        print(user)
        if user is not None:
            login(request, user)
        return redirect('issue_challan')

def logout_(request):
    logout(request)
    return redirect('login')

@permission_required('traffic.challan_form', login_url='login')
def challan_form(request):
    submitted = False
    if request.method=='POST':
        form = ChallanForm(request.POST,request.FILES)    
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('challan_form?submitted=True')
    else:
        form = ChallanForm
        if 'submitted' in request.GET:
            submitted = True
    return render(request,'issuechallan.html',{'form':form,'submitted':submitted})

def e_challan(request):
    return render(request,'aboutechallan.html')
    
def vehicle_details(request):
    if request.method == 'POST':
        value = request.POST.get('value')
        try:
            record = Vehicle.objects.get(Vehicle_Number=value)
            return render(request, 'vehicledetails.html', {'record': record})
        except Vehicle.DoesNotExist:
            return render(request, 'nodetails.html')
    else:
        return render(request, 'vehiclefetch.html')
  

def view_challan(request):
    if request.method == 'POST':
        value = request.POST.get('value')
        try:
            vehicle = Vehicle.objects.get(Vehicle_Number=value)
            record = Issue_Challan.objects.filter(vehicle=vehicle)
            if record:
                return render(request, 'viewchallan.html', {'record': record})
            else:
                return render(request, 'noresults.html')
        except Vehicle.DoesNotExist:
            return render(request, 'noresults.html')
    else:
        return render(request, 'challan_list.html')


def e_contact(request):
    return render(request,'econtact.html')

def challanhistory(request):
    challan=Issue_Challan.objects.all()
    return render(request,'challanhistory.html',{'challan':challan})

def registeredvehicles(request):
    vehi = Vehicle.objects.all()
    return render(request,'regi.html',{'vehi':vehi})

def pay_challan(request):
    if request.method == 'POST':
        value = request.POST.get('value')
        try:
            vehicle = Vehicle.objects.get(Vehicle_Number=value)
            record = Issue_Challan.objects.filter(vehicle=vehicle)
            if record:
                return render(request, 'paychallan.html', {'record': record})
            else:
                return render(request, 'noresults.html')
        except Vehicle.DoesNotExist:
            return render(request, 'noresults.html')
    else:
        return render(request, 'challan_list.html')
    
def vehicle_form(request):
    submitted = False
    if request.method=='POST':
        form = VehicleForm(request.POST,request.FILES)    
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('challan_form?submitted=True')
    else:
        form = VehicleForm
        if 'submitted' in request.GET:
            submitted = True
    return render(request,'issuechallan.html',{'form':form,'submitted':submitted})
